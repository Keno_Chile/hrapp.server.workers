import time
import sys
import traceback
import logging
import select
import threading
import psycopg2
import psycopg2.extensions
import ast
import os
from apscheduler.scheduler import Scheduler
from process_request import ProcessRequest
# from send_notifications import SendNotifications
# from process_scheduler import ProcessSchedule
# from tasks_loader.tasks_loader_sftp import TasksLoaderSFTP
# from utils.directions import Directions
from report_recognition import Recognitions

try:
    logging.basicConfig()
    suds_log = logging.getLogger('suds')
    suds_log.setLevel(logging.ERROR)
    suds_log.propagate = True

    sched = Scheduler()
    #
    pr = ProcessRequest()
    sched.add_interval_job(pr.find, seconds=10)
    #
    # sn = SendNotifications()
    # sched.add_interval_job(sn.send, seconds=10)

    # ps = ProcessSchedule()
    # sched.add_interval_job(ps.schedule, seconds=10)

    # ts = TasksLoaderSFTP()
    # sched.add_interval_job(ts.get, seconds=20)

    sched.start()
    #
    # conn = psycopg2.connect(os.environ['DATABASE_URL'])
    # conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    #
    # curs = conn.cursor()
    # curs.execute("LISTEN route_directions;")

    while True:
        time.sleep(10)
except:
    print "Unexpected error: ", sys.exc_info()
    print traceback.print_exc()
    sys.stdout.flush()
    pass
# finally:
#     conn.close()
