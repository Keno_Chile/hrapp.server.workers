# -*- coding: utf8 -*-
import os
import sys
import threading
import traceback
import psycopg2
import psycopg2.extras
import sql
from report_recognition import Recognitions
# from etl.real_time_snapshots_etl import RealTimeSnapshotsETL

class ProcessRequest:
    def find(self):
        try:
            print 'Searching new processes...'
            sys.stdout.flush()

            conn = psycopg2.connect(os.environ['DATABASE_URL'])

            cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
            cur.execute(sql.PROCESS_REQUEST)

            for record in cur:
                process_id = record.id
                process_type = record.type
                process_params = record.params

                # if process_type == 6:
                #     print 'spawning type 6...'
                #     sys.stdout.flush()
                #     t = threading.Thread(target=InfoDeltas().generate, args=(process_id, process_params))
                #     t.start()

                if process_type == 15:
                    print 'spawning type 15...'
                    sys.stdout.flush()
                    t = threading.Thread(target=Recognitions().generate, args=(process_id, process_params))
                    t.start()
                # elif process_type == 75:
                #     print 'spawning type 75...'
                #     sys.stdout.flush()
                #     t = threading.Thread(target=RealTimeSnapshotsETL().generate, args=(process_id, process_params))
                #     t.start()

            cur.close()
            conn.commit()

        except:
            print "Unexpected error 3: ", sys.exc_info()
            print traceback.print_exc()
            sys.stdout.flush()
            raise
        finally:
            conn.close()
