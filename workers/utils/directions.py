# -*- coding: utf8 -*-
import os
import sys
import psycopg2
import psycopg2.extras
import json
import time
import sql
import traceback
import googlemaps
from datetime import datetime
from datetime import timedelta

class Directions:

    def __init__(self):
        self.gmaps = googlemaps.Client(key=os.environ['GMAPS_KEY'])
        self.unload_time = timedelta(minutes=30)
        self.default_country = 'Chile'
        self.reg_translate = {
            1 : 'I Region',
            2 : 'II Region',
            3 : 'III Region',
            4 : 'IV Region',
            5 : 'V Region',
            6 : 'VI Region',
            7 : 'VII Region',
            8 : 'VIII Region',
            9 : 'IX Region',
            10 : 'X Region',
            11 : 'XI Region',
            12 : 'XII Region',
            13 : 'XIII Region',
            14 : 'XIV Region',
            15 : 'XV Region',
            16 : 'XVI Region'
        }

    def trace(self, route_id, geospatial_data):
        print 'TRACING NEW ROUTE: ', route_id
        print 'FROM: ', geospatial_data
        sys.stdout.flush()

        if 'lat' in geospatial_data:

            try:
                conn = psycopg2.connect(os.environ['DATABASE_URL'])
                cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
                cur1 = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

                start_point = str(geospatial_data['lat']) + ',' + str(geospatial_data['lon'])
                optimize_waypoints = False
                departure_time = datetime.now()
                waypoints = []
                task_ids = []

                geo_stop = False

                cur.execute(sql.FIND_STOPS, [route_id])

                for record in cur:
                    task_ids.append(record.task_id)
                    cur1.execute(sql.UPDATE_STOP_ARRIVAL, [None, record.task_id])
                    if record.geodata is None:
                        geo_stop = True
                    if geo_stop == False:
                        geo = record.geodata.split(',')
                        if len(geo) > 1:
                            waypoints.append(geo[0] + ',' + geo[1])
                        else:
                            geo_stop = True

                cur1.close()

                print 'WAYPOINTS: ', waypoints
                sys.stdout.flush()

                if len(waypoints) > 0:

                    end_point = waypoints.pop()

                    waypoints_result = self.gmaps.directions(start_point,
                                                             end_point,
                                                             waypoints=waypoints,
                                                             optimize_waypoints=optimize_waypoints,
                                                             mode="driving",
                                                             language="es",
                                                             avoid="highways",
                                                             units="metric",
                                                             departure_time=departure_time)

                    legs = waypoints_result[0]["legs"]
                    task_index = 0

                    cur.execute("SELECT now() AT TIME ZONE 'CLT' AS now")
                    now = cur.fetchone().now
                    leg_time = datetime(now.year, now.month, now.day, now.hour, now.minute, now.second)

                    # print "########"
                    for l in legs:
                        leg_time = leg_time + timedelta(seconds=l["duration"]["value"])
                        # print l["start_address"].encode('utf-8')
                        # print l["distance"]["text"].encode('utf-8')
                        # print l["duration"]["text"].encode('utf-8')
                        # print leg_time
                        # print l["end_address"].encode('utf-8')
                        # print task_ids[task_index]
                        # print "########"
                        cur.execute(sql.UPDATE_STOP_ARRIVAL, [leg_time, task_ids[task_index]])
                        leg_time = leg_time + self.unload_time
                        task_index = task_index + 1

                print 'TRACING DONE: ', route_id
                sys.stdout.flush()

                cur.close()
                conn.commit()

            except:
                print "Unexpected error 1: ", sys.exc_info()
                print traceback.print_exc()
                sys.stdout.flush()
                raise

            finally:
                conn.close()

    def optimize(self, route_id, geospatial_data):
        print 'OPTIMIZE NEW ROUTE: ', route_id
        print 'FROM: ', geospatial_data
        sys.stdout.flush()

        if 'lat' in geospatial_data:

            try:
                conn = psycopg2.connect(os.environ['DATABASE_URL'])
                cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
                cur1 = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

                start_point = str(geospatial_data['lat']) + ',' + str(geospatial_data['lon'])
                optimize_waypoints = True
                departure_time = datetime.now()
                waypoints = []
                tasksorder = []
                leftovers = []
                task_ids = []
                task_index = 0

                cur.execute(sql.FIND_STOPS, [route_id])

                for record in cur:
                    task_ids.append(record.task_id)
                    cur1.execute(sql.UPDATE_STOP_ARRIVAL, [None, record.task_id])
                    if record.geodata is None:
                        leftovers.append(record.task_id)
                    else:
                        geo = record.geodata.split(',')
                        if len(geo) > 1:
                            waypoints.append(geo[0] + ',' + geo[1])
                            tasksorder.append(record.task_id)
                        else:
                            leftovers.append(record.task_id)

                cur1.close()

                print 'WAYPOINTS: ', waypoints
                sys.stdout.flush()

                if len(waypoints) > 0:

                    end_point = waypoints.pop()

                    waypoints_result = self.gmaps.directions(start_point,
                                                             end_point,
                                                             waypoints=waypoints,
                                                             optimize_waypoints=optimize_waypoints,
                                                             mode="driving",
                                                             language="es",
                                                             avoid="highways",
                                                             units="metric",
                                                             departure_time=departure_time)

                    legs = waypoints_result[0]["legs"]

                    cur.execute("SELECT now() AT TIME ZONE 'CLT' AS now")
                    now = cur.fetchone().now
                    leg_time = datetime(now.year, now.month, now.day, now.hour, now.minute, now.second)

                    # print "########"
                    for l in legs:
                        leg_time = leg_time + timedelta(seconds=l["duration"]["value"])
                        # print l["start_address"].encode('utf-8')
                        # print l["distance"]["text"].encode('utf-8')
                        # print l["duration"]["text"].encode('utf-8')
                        # print leg_time
                        # print l["end_address"].encode('utf-8')
                        # print task_ids[task_index]
                        # print "########"
                        cur.execute(sql.UPDATE_STOP_AND_ARRIVAL, [task_index, leg_time, tasksorder[task_index]])
                        leg_time = leg_time + self.unload_time
                        task_index = task_index + 1

                for o in leftovers:
                    cur.execute(sql.UPDATE_STOP_AND_ARRIVAL, [task_index, None, o])
                    task_index = task_index + 1

                print 'OPTIMIZE DONE: ', route_id
                sys.stdout.flush()

                cur.close()
                conn.commit()

            except:
                print "Unexpected error 1: ", sys.exc_info()
                print traceback.print_exc()
                sys.stdout.flush()
                raise

            finally:
                conn.close()

    def locate(self):
        print 'LOCATING PLACES STARTING'
        sys.stdout.flush()

        try:
            conn = psycopg2.connect(os.environ['DATABASE_URL'])
            cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
            #cur1 = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

            cur.execute(sql.FIND_UNLOCATED_PLACES, [])

            for record in cur:
                print '-----------------------------'
                if record.address2:
                    address = record.address1 + ', ' + record.city + ', ' + self.reg_translate[int(record.address2)] + ', ' + self.default_country
                else:
                    address = record.address1 + ', ' + self.default_country
                print 'LOC: ' + address
                geocode_result = self.gmaps.geocode(address)
                if len(geocode_result):
                    #print json.dumps(geocode_result)
                    print geocode_result[0]['formatted_address'].encode('utf-8').strip()
                    print json.dumps(geocode_result[0]['geometry']['location'])
                    sys.stdout.flush()

            print 'LOCATING PLACES ENDING'
            sys.stdout.flush()

            #cur1.close()
            cur.close()
            conn.commit()

        except:
            print "Unexpected error 1: ", sys.exc_info()
            print traceback.print_exc()
            sys.stdout.flush()
            raise

        finally:
            conn.close()
