# -*- coding: utf8 -*-
import os
import re
import sys
import xlrd
import psycopg2
import psycopg2.extras
import urlparse
import time
import sql
import uuid
import threading
import traceback
import calendar
import ast
import json
from time import strftime, strptime, mktime, gmtime
from dateutil import parser
from datetime import *
from dateutil.relativedelta import *
import pytz

class ProcessSchedule:
  def schedule(self):
    try:
      print 'Searching new schedules...'
      sys.stdout.flush()

      result = urlparse.urlparse(os.environ['DATABASE_URL'])
      username = result.username
      password = result.password
      database = result.path[1:]
      hostname = result.hostname
      port = result.port
      conn = psycopg2.connect(database = database, user = username, password = password, host = hostname, port = port)

      cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
      tz = pytz.timezone('America/Santiago')
      cur.execute(sql.FIND_PROCESS_SCHEDULES)

      for record in cur:
        ps_id = record.id
        cycle = record.cycle.split(' ')
        params_values = record.params_values
        next_execution = record.next_execution
        now = record.now
        ptype = record.type

        cur1 = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

        if cycle[3] != '*':
          day = int(cycle[3])
          month = now.month
          year = now.year
          delta = relativedelta(months=1)
        elif cycle[4] != '*':
          wd = [SU, MO, TU, WE, TH, FR, SA]
          ndw = now + relativedelta(weekday=wd[int(cycle[4])])
          day = ndw.day
          month = ndw.month
          year = ndw.year
          delta = relativedelta(weeks=1)
        else:
          day = now.day
          month = now.month
          year = now.year
          delta = relativedelta(days=1)

        ne = datetime(year, month, day, int(cycle[1]), int(cycle[0]), 0, 0)
        local_dt = tz.localize(ne, is_dst=True)
        utc_dt = local_dt.astimezone(pytz.utc)

        if not next_execution:
          if utc_dt < now:
            utc_dt = utc_dt + delta
          cur1.execute(sql.UPDATE_PROCESS_SCHEDULES, [utc_dt, ps_id])
        else:
          fromm = utc_dt - delta
          to = utc_dt
          nne = next_execution + delta

          fp = params_values
          fp['from'] = calendar.timegm(fromm.timetuple())
          fp['to'] = calendar.timegm(to.timetuple())

          cur1.execute(sql.CREATE_PROCESS, [ptype, json.dumps(fp)])
          cur1.execute(sql.UPDATE_PROCESS_SCHEDULES, [nne, ps_id])

        cur1.close()

      cur.close()
      conn.commit()

    except:
      print "Unexpected error 3: ", sys.exc_info()
      print traceback.print_exc()
      sys.stdout.flush()
      raise
    finally:
      conn.close()