import time
import sys
import traceback
import logging

from apscheduler.scheduler import Scheduler
from etl.real_time_snapshots_etl import RealTimeSnapshotsETL
from etl.update_delivery_snapshots import UpdateTransactionsETL
#from etl.older_snapshots_etl import OlderSnapshotsETL

try:
    sched = Scheduler()

    rs = RealTimeSnapshotsETL()
    sched.add_interval_job(rs.generate, minutes=5)

    up = UpdateTransactionsETL()
    sched.add_interval_job(up.generate, minutes=20)

    #os = OlderSnapshotsETL()
    #sched.add_interval_job(os.generate, minutes=10)

    sched.start()

    while True:
        time.sleep(10)

except:
    print "Unexpected error: ", sys.exc_info()
    print traceback.print_exc()
    sys.stdout.flush()
    pass
