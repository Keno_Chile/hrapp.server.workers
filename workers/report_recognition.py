# -*- coding: utf8 -*-
import os
import re
import sys
from openpyxl import Workbook, formula
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import NamedStyle, Color, PatternFill, Font, Border, Side
import openpyxl
import psycopg2
import psycopg2.extras
import urlparse
import time
import sql
import sendgrid
import traceback
import uuid
import ubqti
from datetime import datetime
import pytz
import json
from collections import Counter


class Recognitions:
    def generate(self, report_id, report_params):
        try:
            print 'Generating Recognition Report... ' # + str(report_id)
            sys.stdout.flush()

            result = urlparse.urlparse(os.environ['DATABASE_URL'])
            username = result.username
            password = result.password
            database = result.path[1:]
            hostname = result.hostname
            port = result.port

            conn = psycopg2.connect(database=database, user=username, password=password, host=hostname, port=port)
            psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
            psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
            from_date = report_params['from']
            to_date = report_params['to']
            users = report_params['users']

            cur = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
            cur1 = conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
            cur.execute(sql.PROCESS_UPDATE_BEGIN, [report_id])
            conn.commit()

            tz_loc = pytz.timezone('America/Santiago')
            tz_utc = pytz.timezone('UTC')
            from_date_dt = tz_utc.localize(datetime.fromtimestamp(int(from_date)), is_dst=None)
            to_date_dt = tz_utc.localize(datetime.fromtimestamp(int(to_date)), is_dst=None)
            loc_from_date_dt = from_date_dt.astimezone(tz_loc)
            loc_to_date_dt = to_date_dt.astimezone(tz_loc)

            frm_i_date = loc_from_date_dt.strftime('%d-%m-%Y')
            frm_f_date = loc_to_date_dt.strftime('%d-%m-%Y')

            wbk = Workbook()

            bd = Side(style='thin', color="000000")

            hed_sty = NamedStyle(name="hed_sty")
            hed_sty.font = Font(color='000000', bold=True)
            hed_sty.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            hed_sty.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            tab_sty = NamedStyle(name="tab_sty")
            tab_sty.font = Font(color='000000', bold=True)
            tab_sty.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            tab_sty.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            nor_tab = NamedStyle(name="nor_tab")
            nor_tab.font = Font(color='000000', bold=False)
            nor_tab.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            nor_tab.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            red_hed = NamedStyle(name="red_hed")
            red_hed.font = Font(color='FF0000', bold=True)
            red_hed.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            red_hed.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            red_tab = NamedStyle(name="red_tab")
            red_tab.font = Font(color='FF0000', bold=True)
            red_tab.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            red_tab.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            gre_hed = NamedStyle(name="gre_hed")
            gre_hed.font = Font(color='00FF00', bold=True)
            gre_hed.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            gre_hed.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            bas_hed = NamedStyle(name="bas_hed")
            bas_hed.font = Font(color='000000', bold=True)
            bas_hed.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')

            red_txt = NamedStyle(name="red_txt")
            red_txt.font = Font(color='FF0000', bold=False)
            red_txt.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            red_txt.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            gre_txt = NamedStyle(name="gre_txt")
            gre_txt.font = Font(color='00DD00', bold=False)
            gre_txt.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            gre_txt.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            bg_gray = NamedStyle(name="bg_gray")
            bg_gray.fill = PatternFill(fill_type='solid', fgColor='DCDCDC')
            bg_gray.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            bg_white = NamedStyle(name="bg_white")
            bg_white.fill = PatternFill(fill_type='solid', fgColor='FFFFFF')
            bg_white.border = Border(left=bd, right=bd, top=bd, bottom=bd)

            wbk.add_named_style(hed_sty)
            wbk.add_named_style(tab_sty)
            wbk.add_named_style(red_hed)
            wbk.add_named_style(red_tab)
            wbk.add_named_style(bas_hed)
            wbk.add_named_style(nor_tab)
            wbk.add_named_style(gre_hed)
            wbk.add_named_style(red_txt)
            wbk.add_named_style(gre_txt)
            wbk.add_named_style(bg_gray)
            wbk.add_named_style(bg_white)
            sheet = wbk.create_sheet(u'Resumen General-Datos por Área', 1)
            sheet1 = wbk.create_sheet(u'Empleados', 2)

            old_employee = ''
            cur.execute(sql.GET_ALL_COMPETENCIES)
            comp = json.loads(json.dumps(cur.fetchall()))

            old_area = ''
            total_like = 0
            total_dislike = 0
            area = []
            employee = []
            comps = {}
            cont_a = 0
            cont2_a = 0
            cont_e = 0
            cont2_e = 0
            area_data = []
            employee_data = []

            for user in users:
                print user
                sys.stdout.flush()
                cur.execute(sql.GET_WEB_REPORT_USER_INFO, [user,])
                print cur.rowcount
                sys.stdout.flush()
                rec = cur.fetchone()
                email = rec.email
                company_id = rec.company_id
                report_name = rec.name+' '+rec.last_name
                record_count = 0

                # ### DATOS AREA (empresa)

                cur.execute(sql.RECOGNITION_REPORT_AREA, [from_date, to_date, company_id])
                record_count += cur.rowcount

                for record in cur:
                    company = record.company
                    dislike = record.dislike if record.dislike is not None else 0
                    like = record.like if record.like is not None else 0
                    area_code = record.area_code
                    area_name = record.area_name
                    competency_name = record.competency_name

                    area_data.append({'company': company, 'dislike': dislike, 'like': like, 'area_code': area_code,
                                      'area_name': area_name, 'competency_name': competency_name})

                for com in comp:
                    comps[com[0]] = {'likes': 0, 'dislikes': 0}

                for detarea in area_data:
                    # AREAS
                    total_like += detarea['like']
                    total_dislike += detarea['dislike']
                    cont2_a += 1
                    if old_area == '':
                        old_area = detarea['area_name']
                    if old_area != detarea['area_name']:
                        area.append([{old_area: cont_a}])
                        old_area = detarea['area_name']
                        cont_a = 0
                    elif cont2_a == len(area_data):
                        cont_a += 1
                        area.append([{old_area: cont_a}])
                    cont_a += 1

                    # COMPETENCIES

                    for compet in comp:
                        if unicode(detarea['competency_name']) == compet[0]:
                            comps[compet[0]]['likes'] += detarea['like']
                            comps[compet[0]]['dislikes'] += detarea['dislike']

                # hoja1_cols = ['C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                #               'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AB', 'AC', 'AD']

                total = total_like + total_dislike

                # RESUMEN

                sheet['A1'] = 'Likes'
                sheet['A1'].style = 'gre_hed'
                sheet.column_dimensions['A'].width = 18

                sheet['A2'] = 'Dislikes'
                sheet['A2'].style = 'red_hed'

                sheet['B1'] = float(total_like) / float(total)
                sheet['B1'].style = 'gre_hed'
                sheet.column_dimensions['B'].width = 30
                sheet['B1'].number_format = '0.00%'

                sheet['B2'] = float(total_dislike) / float(total)
                sheet['B2'].style = 'red_hed'
                sheet['B2'].number_format = '0.00%'

                sheet.column_dimensions['C'].width = 15
                sheet.column_dimensions['D'].width = 15
                sheet.column_dimensions['E'].width = 15
                sheet.column_dimensions['F'].width = 15

                rowcont = 1

                for ref in comps:
                    sheet.merge_cells('D'+str(rowcont)+':E'+str(rowcont))
                    sheet['D'+str(rowcont)] = ref
                    sheet['D'+str(rowcont)].style = 'hed_sty'
                    sheet['E'+str(rowcont)].style = 'hed_sty'
                    sheet['D'+str(rowcont+1)] = comps[ref]['dislikes']
                    sheet['D'+str(rowcont+1)].style = 'red_txt'
                    sheet['E'+str(rowcont+1)] = comps[ref]['likes']
                    sheet['E'+str(rowcont+1)].style = 'gre_txt'
                    rowcont += 2

                controws = rowcont + 2  # FILA INICIAL PARA DATA POR AREA

                # AREAS

                sheet['A'+str(controws)] = 'Área'
                sheet['A'+str(controws)].style = 'hed_sty'

                sheet['B'+str(controws)] = 'Competencia'
                sheet['B'+str(controws)].style = 'hed_sty'

                sheet['C'+str(controws)] = 'Likes'
                sheet['C'+str(controws)].style = 'hed_sty'

                sheet['D'+str(controws)] = 'Dislikes'
                sheet['D'+str(controws)].style = 'hed_sty'

                sheet['E'+str(controws)] = 'Total Likes'
                sheet['E'+str(controws)].style = 'hed_sty'

                sheet['F'+str(controws)] = 'Total Dislikes'
                sheet['F'+str(controws)].style = 'hed_sty'

                controws += 1

                area_length = 0
                max_row = controws + area_length
                previous_area = ''

                bgcol = 1
                bgfill = ''

                largos = Counter(k['area_name'] for k in area_data if k.get('area_name'))
                for data in area_data:
                    current_area = data['area_name']
                    if previous_area != current_area:
                        if bgcol == 0:
                            bgfill = 'bg_white'
                            bgcol = 1
                        elif bgcol == 1:
                            bgfill = 'bg_gray'
                            bgcol = 0
                        area_length = largos[data['area_name']]
                        max_row = controws + area_length - 1
                        sheet.merge_cells('A'+str(controws)+':A'+str(max_row))
                        sheet['A'+str(controws)] = data['area_name']
                        sheet['A' + str(controws)].style = 'nor_tab'
                        sheet['A' + str(controws)].style = bgfill
                        sheet.merge_cells('E' + str(controws)+':E'+str(max_row))
                        sheet['E' + str(controws)] = '=SUM(C' + str(controws)+':C'+str(max_row)+')'
                        sheet['E' + str(controws)].style = 'nor_tab'
                        sheet['E' + str(controws)].style = bgfill
                        sheet.merge_cells('F' + str(controws)+':F'+str(max_row))
                        sheet['F' + str(controws)] = '=SUM(D' + str(controws)+':D'+str(max_row)+')'
                        sheet['F' + str(controws)].style = 'nor_tab'
                        sheet['F' + str(controws)].style = bgfill
                        previous_area = current_area
                    sheet['B' + str(controws)] = data['competency_name']
                    sheet['B' + str(controws)].style = 'nor_tab'
                    sheet['B' + str(controws)].style = bgfill

                    sheet['C' + str(controws)] = data['like']
                    sheet['C' + str(controws)].style = 'nor_tab'
                    sheet['C' + str(controws)].style = bgfill

                    sheet['D' + str(controws)] = data['dislike']
                    sheet['D' + str(controws)].style = 'nor_tab'
                    sheet['D' + str(controws)].style = bgfill

                    controws += 1

                # #######  EMPLOYEES  ###################################

                cur.execute(sql.RECOGNITION_REPORT_EMPLOYEE, [from_date, to_date, company_id])

                for record in cur:
                    company = record.company
                    dislike = record.dislike if record.dislike is not None else 0
                    like = record.like if record.like is not None else 0
                    user_name = record.user_name
                    competency_name = record.competency_name

                    employee_data.append({'company': company, 'dislike': dislike, 'like': like,
                                          'user_name': user_name, 'competency_name': competency_name})

                for com in comp:
                    comps[com[0]] = {'likes': 0, 'dislikes': 0}

                for detemp in employee_data:
                    # EMPLOYEES
                    total_like += detemp['like']
                    total_dislike += detemp['dislike']
                    cont2_e += 1
                    if old_employee == '':
                        old_employee = detemp['user_name']
                    if old_employee != detemp['user_name']:
                        employee.append([{old_employee: cont_e}])
                        old_employee = detemp['user_name']
                        cont_e = 0
                    elif cont2_a == len(employee_data):
                        cont_e += 1
                        employee.append([{old_employee: cont_e}])
                    cont_e += 1

                    # COMPETENCIES

                    for compet in comp:
                        if unicode(detarea['competency_name']) == compet[0]:
                            comps[compet[0]]['likes'] += detarea['like']
                            comps[compet[0]]['dislikes'] += detarea['dislike']

                sheet1['A1'] = 'Empleado'
                sheet1['A1'].style = 'hed_sty'

                sheet1['B1'] = 'Competencia'
                sheet1['B1'].style = 'hed_sty'

                sheet1['C1'] = 'Likes'
                sheet1['C1'].style = 'hed_sty'

                sheet1['D1'] = 'Dislikes'
                sheet1['D1'].style = 'hed_sty'

                sheet1['E1'] = 'Total Likes'
                sheet1['E1'].style = 'hed_sty'

                sheet1['F1'] = 'Total Dislikes'
                sheet1['F1'].style = 'hed_sty'

                sheet1.column_dimensions['A'].width = 18
                sheet1.column_dimensions['B'].width = 30
                sheet1.column_dimensions['C'].width = 8
                sheet1.column_dimensions['D'].width = 8
                sheet1.column_dimensions['E'].width = 11
                sheet1.column_dimensions['F'].width = 11

                controws = 2  # FILA INICIAL PARA DATA POR AREA

                emp_length = 0
                max_row = controws + emp_length
                previous_emp = ''

                bgcol = 0
                bgfill = ''

                largos = Counter(k['user_name'] for k in employee_data if k.get('user_name'))
                for data in employee_data:
                    sys.stdout.flush()
                    current_emp = data['user_name']
                    if previous_emp != current_emp:
                        if bgcol == 0:
                            bgfill = 'bg_white'
                            bgcol = 1
                        elif bgcol == 1:
                            bgfill = 'bg_gray'
                            bgcol = 0
                        emp_length = largos[data['user_name']]
                        max_row = controws + emp_length - 1
                        sheet1.merge_cells('A' + str(controws) + ':A' + str(max_row))
                        sheet1['A' + str(controws)] = data['user_name']
                        sheet1['A' + str(controws)].style = 'nor_tab'
                        sheet1['A' + str(controws)].style = bgfill
                        sheet1.merge_cells('E' + str(controws) + ':E' + str(max_row))
                        sheet1['E' + str(controws)] = '=SUM(C' + str(controws) + ':C' + str(max_row) + ')'
                        sheet1['E' + str(controws)].style = 'nor_tab'
                        sheet1['E' + str(controws)].style = bgfill
                        sheet1.merge_cells('F' + str(controws) + ':F' + str(max_row))
                        sheet1['F' + str(controws)] = '=SUM(D' + str(controws) + ':D' + str(max_row) + ')'
                        sheet1['F' + str(controws)].style = 'nor_tab'
                        sheet1['F' + str(controws)].style = bgfill
                        previous_emp = current_emp
                    sheet1['B' + str(controws)] = data['competency_name']
                    sheet1['B' + str(controws)].style = 'nor_tab'
                    sheet1['B' + str(controws)].style = bgfill

                    sheet1['C' + str(controws)] = data['like']
                    sheet1['C' + str(controws)].style = 'nor_tab'
                    sheet1['C' + str(controws)].style = bgfill

                    sheet1['D' + str(controws)] = data['dislike']
                    sheet1['D' + str(controws)].style = 'nor_tab'
                    sheet1['D' + str(controws)].style = bgfill

                    controws += 1

            rem = wbk.get_sheet_by_name('Sheet')
            wbk.remove_sheet(rem)

            wbk.save('uploads/' + str(report_id) + '.xlsx')

            s = sendgrid.Sendgrid(os.environ['SENDGRID_USERNAME'], os.environ['SENDGRID_PASSWORD'], secure=True)
            message = sendgrid.Message('HRAPP@ubqti.com', 'UBQTi - Reporte de Reconocimiento',
                                       u'Estimad@ '+report_name+u' \nSegún lo solicitado por medio de la Aplicación Web, '
                                       u'se adjunta reporte de Reconocimiento.\n\nAtte \n\n UBQTeam')
            message.add_to(email)

            file_name = 'reconocimiento_'+frm_i_date+'_'+frm_f_date+'.xlsx'
            message.add_attachment(file_name, 'uploads/' + str(report_id) + '.xlsx')
            s.smtp.send(message)

            print 'Report sent'
            sys.stdout.flush()

            # cur.execute(sql.PROCESS_UPDATE_FINISH, [report_id]);

            cur.close()
            cur1.close()
            conn.commit()
        except:
            raise
        finally:
            conn.close()
