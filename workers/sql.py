CREATE_USER = \
    "INSERT INTO users (id, profiles, name, last_name, properties, secure, audit, is_active, is_deleted, company_id) \
     VALUES (uuid_generate_v4(), %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id AS user_id"

GENERAL_TABLES = \
    'SELECT * \
       FROM general_tables \
      WHERE code = %s'

GET_COUNTRIES_INFO = \
    'SELECT * FROM COUNTRIES WHERE id IN %s'

CREATE_PROCESS = \
    'INSERT INTO processes (id, type, status, request_date, params) \
     VALUES (uuid_generate_v4(), %s, 0, NOW(), %s)'

FIND_PROCESS_SCHEDULES = "SELECT a.id, a.cycle, a.params_values, a.next_execution, NOW() AS now, b.type \
                            FROM process_schedules a, process_types b \
                           WHERE b.id = a.process_type_id \
                             AND a.is_active = TRUE \
                             AND (a.next_execution IS NULL \
                              OR a.next_execution < NOW())"

UPDATE_PROCESS_SCHEDULES = 'UPDATE process_schedules \
                               SET next_execution = %s \
                             WHERE id = %s'

PROCESS_REQUEST = 'SELECT id, type, params \
                  FROM processes a \
                 WHERE status = 0'

PROCESS_UPDATE_BEGIN = 'UPDATE processes \
                          SET status = 1, \
                              begin_date = now() \
                        WHERE id = %s'

PROCESS_UPDATE_FINISH = 'UPDATE processes \
                           SET status = 2, \
                               finish_date = now() \
                         WHERE id = %s'

PROCESS_UPDATE_ERROR = 'UPDATE processes \
                          SET status = 3 \
                        WHERE id = %s'

GET_USER_INFO = \
  "SELECT u.id AS user_id, u.name, u.last_name, u.is_active AS user_is_active, u.properties, u.secure, u.audit, \
          pro.id AS profile_id, c.id AS company_id, c.name AS company_name, c.is_active AS company_is_active, \
          pro.privileges \
     FROM users u, companies c, profiles pro, privileges pri \
    WHERE c.id = u.company_id \
      AND u.is_deleted = FALSE \
      AND pro.id = ANY(u.profiles) \
      AND pri.code IN (SELECT json_object_keys(pro1.privileges) AS o \
                         FROM profiles pro1 \
                        WHERE pro1.id = pro.id) \
      AND u.id = %s \
    LIMIT 1"

COMPANIES_FIND = \
    "SELECT id \
       FROM companies \
      WHERE ax_id = %s"

COMPANIES_CREATE = \
    "INSERT INTO companies (id, name, is_active, legal_name, ax_id, taxid, contact_email, country_id) \
     VALUES (uuid_generate_v4(), %s, TRUE, %s, %s, %s, %s, (SELECT id FROM countries WHERE ax_id = %s))"

COMPANIES_UPDATE = \
    "UPDATE companies \
        SET name = %s, \
            legal_name = %s, \
            taxid = %s, \
            contact_email = %s, \
            country_id = (SELECT id FROM countries WHERE ax_id = %s) \
      WHERE id = %s"

GENERAL_TABLES_UPDATE_VALUES = \
    "UPDATE general_tables \
        SET values = %s \
      WHERE code = %s"

GENERAL_TABLES_CREATE = \
    "INSERT INTO general_tables (id, code, name, values, is_active) \
     VALUES (uuid_generate_v4(), %s, %s, %s, TRUE) RETURNING id"

CITIES_FIND = \
    "SELECT ci.id \
       FROM cities ci \
      WHERE ci.name = %s"

CITIES_CREATE = \
    "INSERT INTO cities (id, name, is_active, country_id, timezone, region) \
     VALUES (uuid_generate_v4(), %s, TRUE, (SELECT id FROM countries WHERE name = 'Chile'), 'America/Santiago', NULL) \
  RETURNING id"

PLACES_FIND = \
    "SELECT p.id, p.info \
       FROM places p \
      WHERE p.copec_code = %s"

PLACES_CREATE = \
    "INSERT INTO places (id, copec_code, name, type, address1, address2, is_active, info, city_id, company_id) \
     VALUES (uuid_generate_v4(), %s, %s, 1, %s, %s, TRUE, %s, %s, %s) \
  RETURNING id, info"

PLACES_UPDATE = \
    "UPDATE places \
        SET name = %s, \
            address1 = %s, \
            address2 = %s, \
            info = %s, \
            city_id = %s \
      WHERE id = %s"

# ####################### REPORTES #######################

RECOGNITION_REPORT_AREA = \
"with datag as( with comp as( with jsondat as (select * from json_each((select info from categories where code = 'COMPETENCIES')::JSON) as dat) \
select key::SMALLINT as val, value->'dev'->>'name' as name from jsondat) \
    SELECT c1.name company, r1.result, \
                    (CASE WHEN u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' IS NULL \
                                THEN u1.properties->(SELECT id from profiles where type = 'WEB_ADMIN_PROFILE')::TEXT->>'area' \
                                ELSE u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' END) as  area_code, \
                 (SELECT c3.info-> \
                            (CASE WHEN u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' IS NULL \
                                THEN u1.properties->(SELECT id from profiles where type = 'WEB_ADMIN_PROFILE')::TEXT->>'area' \
                                ELSE u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' END)::TEXT->'es'->>'name' from categories c3 where c3.code = 'DEPARTMENTS' ) as area_name, \
                 comp.name as competency_name \
    from recognitions r1, users u1, companies c1, comp, users u2 \
    where u1.id = r1.recognizee_id \
    and u1.company_id in (SELECT  c5.id FROM companies c5 \
                            where c5.id = u2.company_id \
                                 or c5.parent_id = u2.company_id \
                                 or c5.parent_id in (select c4.id from companies c4 where c4.parent_id = u2.company_id)) \
    and u2.id = r1.recognizer_id \
    and c1.id = u1.company_id \
    and comp.val = r1.competency_id \
    and extract('epoch' from r1.date) between (%s)::BIGINT and (%s)::BIGINT \
    and u2.company_id  = %s) \
select d.company, d.area_code, d.area_name, d.competency_name, \
	(select count(result) from datag where result = 0 and d.company = datag.company and d.area_code = datag.area_code and d.competency_name = datag.competency_name group by datag.company, datag.area_code, datag.competency_name)::INT as dislike, \
	(select count(result) from datag where result = 1 and d.company = datag.company and d.area_code = datag.area_code and d.competency_name = datag.competency_name group by datag.company, datag.area_code, datag.competency_name)::INT as like \
 from datag d group by 1,2,3,4 order by 2"
# --

RECOGNITION_REPORT_EMPLOYEE = \
"with datag as(with comp as(with jsondat as (select * from json_each((select info from categories where code = 'COMPETENCIES')::JSON) as dat)\
	select key::SMALLINT as val, value->'dev'->>'name' as name from jsondat) \
	SELECT c1.name company, r1.result, u1.name||' '||u1.last_name as user_name, \
				 (SELECT c3.info-> \
                        (CASE WHEN u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' IS NULL \
                            THEN u1.properties->(SELECT id from profiles where type = 'WEB_ADMIN_PROFILE')::TEXT->>'area' \
                            ELSE u1.properties->(SELECT id from profiles where type = 'MOBILE_PROFILE')::TEXT->>'area' END)::TEXT->'es'->>'name' from categories c3 where c3.code = 'DEPARTMENTS' ) as area_name, \
				 comp.name as competency_name \
	from recognitions r1, users u1, companies c1, comp, users u2 \
	where u1.id = r1.recognizee_id \
	and u1.company_id in (SELECT  c5.id FROM companies c5 \
                                where c5.id = u2.company_id \
                                     or c5.parent_id = u2.company_id \
                                     or c5.parent_id in (select c4.id from companies c4 where c4.parent_id = u2.company_id)) \
	and u2.id = r1.recognizer_id \
	and c1.id = u1.company_id \
	and comp.val = r1.competency_id \
    and extract('epoch' from r1.date) between (%s)::BIGINT and (%s)::BIGINT \
	and u2.company_id  = %s) \
select d.company, d.user_name, d.competency_name, \
	(select count(result) from datag where result = 0 and d.company = datag.company and d.user_name = datag.user_name and d.competency_name = datag.competency_name group by datag.company, datag.user_name, datag.competency_name)::INT as dislike, \
	(select count(result) from datag where result = 1 and d.company = datag.company and d.user_name = datag.user_name and d.competency_name = datag.competency_name group by datag.company, datag.user_name, datag.competency_name)::INT as like \
 from datag d \
group by 1,2,3 \
order by 2  "
# --
GET_ALL_COMPETENCIES =\
"with comp as(with jsondat as (select * from json_each((select info from categories where code = 'COMPETENCIES')::JSON)) \
                select key::SMALLINT as val, value->'dev'->>'name' as name from jsondat) \
select name from comp "

GET_WEB_REPORT_USER_INFO = \
   "SELECT u.id AS user_id, u.name, u.last_name, u.is_active AS user_is_active, \
           u.properties->pro.id::TEXT->>'email' as email, pro.id AS profile_id, u.company_id \
     FROM users u, profiles pro, privileges pri \
    WHERE u.is_deleted = FALSE \
      AND pro.id = ANY(u.profiles) \
	  AND pro.type LIKE '%%'||'ADMIN'||'%%' \
      AND pri.code IN (SELECT json_object_keys(pro1.privileges) AS o \
                         FROM profiles pro1 \
                        WHERE pro1.id = pro.id) \
      AND u.id = %s \
    LIMIT 1 "

